/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Aya
 */
@Entity
@Table(name = "notifications_has_vendor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotificationsHasVendor.findAll", query = "SELECT n FROM NotificationsHasVendor n"),
    @NamedQuery(name = "NotificationsHasVendor.findByNotificationsNotificationsId", query = "SELECT n FROM NotificationsHasVendor n WHERE n.notificationsHasVendorPK.notificationsNotificationsId = :notificationsNotificationsId"),
    @NamedQuery(name = "NotificationsHasVendor.findByVendorVendorid", query = "SELECT n FROM NotificationsHasVendor n WHERE n.notificationsHasVendorPK.vendorVendorid = :vendorVendorid")})
public class NotificationsHasVendor implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NotificationsHasVendorPK notificationsHasVendorPK;
    @JoinColumn(name = "vendor_vendorid", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Vendor vendor;

    public NotificationsHasVendor() {
    }

    public NotificationsHasVendor(NotificationsHasVendorPK notificationsHasVendorPK) {
        this.notificationsHasVendorPK = notificationsHasVendorPK;
    }

    public NotificationsHasVendor(int notificationsNotificationsId, int vendorVendorid) {
        this.notificationsHasVendorPK = new NotificationsHasVendorPK(notificationsNotificationsId, vendorVendorid);
    }

    public NotificationsHasVendorPK getNotificationsHasVendorPK() {
        return notificationsHasVendorPK;
    }

    public void setNotificationsHasVendorPK(NotificationsHasVendorPK notificationsHasVendorPK) {
        this.notificationsHasVendorPK = notificationsHasVendorPK;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (notificationsHasVendorPK != null ? notificationsHasVendorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotificationsHasVendor)) {
            return false;
        }
        NotificationsHasVendor other = (NotificationsHasVendor) object;
        if ((this.notificationsHasVendorPK == null && other.notificationsHasVendorPK != null) || (this.notificationsHasVendorPK != null && !this.notificationsHasVendorPK.equals(other.notificationsHasVendorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "interfaces.NotificationsHasVendor[ notificationsHasVendorPK=" + notificationsHasVendorPK + " ]";
    }
    
}
