/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Aya
 */
@Embeddable
public class VendorPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private int id;
    @Basic(optional = false)
    @Column(name = "vendor_mobile_id", nullable = false)
    private int vendorMobileId;

    public VendorPK() {
    }

    public VendorPK(int id, int vendorMobileId) {
        this.id = id;
        this.vendorMobileId = vendorMobileId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVendorMobileId() {
        return vendorMobileId;
    }

    public void setVendorMobileId(int vendorMobileId) {
        this.vendorMobileId = vendorMobileId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) vendorMobileId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VendorPK)) {
            return false;
        }
        VendorPK other = (VendorPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.vendorMobileId != other.vendorMobileId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "interfaces.VendorPK[ id=" + id + ", vendorMobileId=" + vendorMobileId + " ]";
    }
    
}
