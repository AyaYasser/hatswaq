/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Aya
 */
@Entity
@Table(name = "offer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Offer.findAll", query = "SELECT o FROM Offer o"),
    @NamedQuery(name = "Offer.findById", query = "SELECT o FROM Offer o WHERE o.id = :id"),
    @NamedQuery(name = "Offer.findByDescription", query = "SELECT o FROM Offer o WHERE o.description = :description"),
    @NamedQuery(name = "Offer.findByIcon", query = "SELECT o FROM Offer o WHERE o.icon = :icon"),
    @NamedQuery(name = "Offer.findByStartDate", query = "SELECT o FROM Offer o WHERE o.startDate = :startDate"),
    @NamedQuery(name = "Offer.findByEndDate", query = "SELECT o FROM Offer o WHERE o.endDate = :endDate"),
    @NamedQuery(name = "Offer.findByStatus", query = "SELECT o FROM Offer o WHERE o.status = :status"),
    @NamedQuery(name = "Offer.findByRate", query = "SELECT o FROM Offer o WHERE o.rate = :rate"),
    @NamedQuery(name = "Offer.findByAddedBy", query = "SELECT o FROM Offer o WHERE o.addedBy = :addedBy"),
    @NamedQuery(name = "Offer.findByPinding", query = "SELECT o FROM Offer o WHERE o.pinding = :pinding")})
public class Offer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "description", length = 45)
    private String description;
    @Basic(optional = false)
    @Column(name = "icon", nullable = false, length = 100)
    private String icon;
    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @Column(name = "status")
    private Boolean status;
    @Column(name = "rate")
    private Integer rate;
    @Basic(optional = false)
    @Column(name = "added_by", nullable = false)
    private boolean addedBy;
    @Basic(optional = false)
    @Column(name = "pinding", nullable = false)
    private boolean pinding;
    @JoinColumn(name = "vendor_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Vendor vendorId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "offer")
    private List<Item> itemList;

    public Offer() {
    }

    public Offer(Integer id) {
        this.id = id;
    }

    public Offer(Integer id, String icon, boolean addedBy, boolean pinding) {
        this.id = id;
        this.icon = icon;
        this.addedBy = addedBy;
        this.pinding = pinding;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public boolean getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(boolean addedBy) {
        this.addedBy = addedBy;
    }

    public boolean getPinding() {
        return pinding;
    }

    public void setPinding(boolean pinding) {
        this.pinding = pinding;
    }

    public Vendor getVendorId() {
        return vendorId;
    }

    public void setVendorId(Vendor vendorId) {
        this.vendorId = vendorId;
    }

    @XmlTransient
    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Offer)) {
            return false;
        }
        Offer other = (Offer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "interfaces.Offer[ id=" + id + " ]";
    }
    
}
