/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Aya
 */
@Entity
@Table(name = "resources_details")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ResourcesDetails.findAll", query = "SELECT r FROM ResourcesDetails r"),
    @NamedQuery(name = "ResourcesDetails.findById", query = "SELECT r FROM ResourcesDetails r WHERE r.id = :id"),
    @NamedQuery(name = "ResourcesDetails.findByFullOfferPage", query = "SELECT r FROM ResourcesDetails r WHERE r.fullOfferPage = :fullOfferPage"),
    @NamedQuery(name = "ResourcesDetails.findByItemImage", query = "SELECT r FROM ResourcesDetails r WHERE r.itemImage = :itemImage")})
public class ResourcesDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "full_offer_page", nullable = false, length = 100)
    private String fullOfferPage;
    @Basic(optional = false)
    @Column(name = "item_image", nullable = false, length = 100)
    private String itemImage;
    @JoinColumn(name = "item_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Item itemId;

    public ResourcesDetails() {
    }

    public ResourcesDetails(Integer id) {
        this.id = id;
    }

    public ResourcesDetails(Integer id, String fullOfferPage, String itemImage) {
        this.id = id;
        this.fullOfferPage = fullOfferPage;
        this.itemImage = itemImage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullOfferPage() {
        return fullOfferPage;
    }

    public void setFullOfferPage(String fullOfferPage) {
        this.fullOfferPage = fullOfferPage;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public Item getItemId() {
        return itemId;
    }

    public void setItemId(Item itemId) {
        this.itemId = itemId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ResourcesDetails)) {
            return false;
        }
        ResourcesDetails other = (ResourcesDetails) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "interfaces.ResourcesDetails[ id=" + id + " ]";
    }
    
}
