/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Aya
 */
@Embeddable
public class NotificationsHasVendorPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "notifications_notifications_id", nullable = false)
    private int notificationsNotificationsId;
    @Basic(optional = false)
    @Column(name = "vendor_vendorid", nullable = false)
    private int vendorVendorid;

    public NotificationsHasVendorPK() {
    }

    public NotificationsHasVendorPK(int notificationsNotificationsId, int vendorVendorid) {
        this.notificationsNotificationsId = notificationsNotificationsId;
        this.vendorVendorid = vendorVendorid;
    }

    public int getNotificationsNotificationsId() {
        return notificationsNotificationsId;
    }

    public void setNotificationsNotificationsId(int notificationsNotificationsId) {
        this.notificationsNotificationsId = notificationsNotificationsId;
    }

    public int getVendorVendorid() {
        return vendorVendorid;
    }

    public void setVendorVendorid(int vendorVendorid) {
        this.vendorVendorid = vendorVendorid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) notificationsNotificationsId;
        hash += (int) vendorVendorid;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotificationsHasVendorPK)) {
            return false;
        }
        NotificationsHasVendorPK other = (NotificationsHasVendorPK) object;
        if (this.notificationsNotificationsId != other.notificationsNotificationsId) {
            return false;
        }
        if (this.vendorVendorid != other.vendorVendorid) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "interfaces.NotificationsHasVendorPK[ notificationsNotificationsId=" + notificationsNotificationsId + ", vendorVendorid=" + vendorVendorid + " ]";
    }
    
}
