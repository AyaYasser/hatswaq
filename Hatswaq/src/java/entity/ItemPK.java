/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Aya
 */
@Embeddable
public class ItemPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private int id;
    @Basic(optional = false)
    @Column(name = "offer_id", nullable = false)
    private int offerId;

    public ItemPK() {
    }

    public ItemPK(int id, int offerId) {
        this.id = id;
        this.offerId = offerId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) offerId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemPK)) {
            return false;
        }
        ItemPK other = (ItemPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.offerId != other.offerId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "interfaces.ItemPK[ id=" + id + ", offerId=" + offerId + " ]";
    }
    
}
