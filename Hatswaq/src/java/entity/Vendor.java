/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Aya
 */
@Entity
@Table(name = "vendor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vendor.findAll", query = "SELECT v FROM Vendor v"),
    @NamedQuery(name = "Vendor.findById", query = "SELECT v FROM Vendor v WHERE v.vendorPK.id = :id"),
    @NamedQuery(name = "Vendor.findByName", query = "SELECT v FROM Vendor v WHERE v.name = :name"),
    @NamedQuery(name = "Vendor.findByLogo", query = "SELECT v FROM Vendor v WHERE v.logo = :logo"),
    @NamedQuery(name = "Vendor.findByRepresentativeName", query = "SELECT v FROM Vendor v WHERE v.representativeName = :representativeName"),
    @NamedQuery(name = "Vendor.findByUsername", query = "SELECT v FROM Vendor v WHERE v.username = :username"),
    @NamedQuery(name = "Vendor.findByPassword", query = "SELECT v FROM Vendor v WHERE v.password = :password"),
    @NamedQuery(name = "Vendor.findByEnabled", query = "SELECT v FROM Vendor v WHERE v.enabled = :enabled"),
    @NamedQuery(name = "Vendor.findByVendorMobileId", query = "SELECT v FROM Vendor v WHERE v.vendorPK.vendorMobileId = :vendorMobileId"),
    @NamedQuery(name = "Vendor.findByDescription", query = "SELECT v FROM Vendor v WHERE v.description = :description")})
public class Vendor implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected VendorPK vendorPK;
    @Column(name = "name", length = 20)
    private String name;
    @Basic(optional = false)
    @Column(name = "logo", nullable = false, length = 100)
    private String logo;
    @Column(name = "representative_name", length = 45)
    private String representativeName;
    @Column(name = "username", length = 45)
    private String username;
    @Column(name = "password", length = 45)
    private String password;
    @Basic(optional = false)
    @Column(name = "enabled", nullable = false)
    private int enabled;
    @Basic(optional = false)
    @Column(name = "description", nullable = false, length = 200)
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vendorId")
    private List<Offer> offerList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vendorId")
    private List<AdminCommunicateVendor> adminCommunicateVendorList;
    @JoinColumn(name = "vendor_mobile_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private VendorMobile vendorMobile;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vendorId")
    private List<Branch> branchList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vendor")
    private List<NotificationsHasVendor> notificationsHasVendorList;

    public Vendor() {
    }

    public Vendor(VendorPK vendorPK) {
        this.vendorPK = vendorPK;
    }

    public Vendor(VendorPK vendorPK, String logo, int enabled, String description) {
        this.vendorPK = vendorPK;
        this.logo = logo;
        this.enabled = enabled;
        this.description = description;
    }

    public Vendor(int id, int vendorMobileId) {
        this.vendorPK = new VendorPK(id, vendorMobileId);
    }

    public VendorPK getVendorPK() {
        return vendorPK;
    }

    public void setVendorPK(VendorPK vendorPK) {
        this.vendorPK = vendorPK;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getRepresentativeName() {
        return representativeName;
    }

    public void setRepresentativeName(String representativeName) {
        this.representativeName = representativeName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public List<Offer> getOfferList() {
        return offerList;
    }

    public void setOfferList(List<Offer> offerList) {
        this.offerList = offerList;
    }

    @XmlTransient
    public List<AdminCommunicateVendor> getAdminCommunicateVendorList() {
        return adminCommunicateVendorList;
    }

    public void setAdminCommunicateVendorList(List<AdminCommunicateVendor> adminCommunicateVendorList) {
        this.adminCommunicateVendorList = adminCommunicateVendorList;
    }

    public VendorMobile getVendorMobile() {
        return vendorMobile;
    }

    public void setVendorMobile(VendorMobile vendorMobile) {
        this.vendorMobile = vendorMobile;
    }

    @XmlTransient
    public List<Branch> getBranchList() {
        return branchList;
    }

    public void setBranchList(List<Branch> branchList) {
        this.branchList = branchList;
    }

    @XmlTransient
    public List<NotificationsHasVendor> getNotificationsHasVendorList() {
        return notificationsHasVendorList;
    }

    public void setNotificationsHasVendorList(List<NotificationsHasVendor> notificationsHasVendorList) {
        this.notificationsHasVendorList = notificationsHasVendorList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vendorPK != null ? vendorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vendor)) {
            return false;
        }
        Vendor other = (Vendor) object;
        if ((this.vendorPK == null && other.vendorPK != null) || (this.vendorPK != null && !this.vendorPK.equals(other.vendorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "interfaces.Vendor[ vendorPK=" + vendorPK + " ]";
    }
    
}
