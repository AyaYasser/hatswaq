/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Aya
 */
@Entity
@Table(name = "items_list")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ItemsList.findAll", query = "SELECT i FROM ItemsList i"),
    @NamedQuery(name = "ItemsList.findById", query = "SELECT i FROM ItemsList i WHERE i.id = :id"),
    @NamedQuery(name = "ItemsList.findByName", query = "SELECT i FROM ItemsList i WHERE i.name = :name"),
    @NamedQuery(name = "ItemsList.findByDescription", query = "SELECT i FROM ItemsList i WHERE i.description = :description"),
    @NamedQuery(name = "ItemsList.findByIcon", query = "SELECT i FROM ItemsList i WHERE i.icon = :icon")})
public class ItemsList implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "name", length = 45)
    private String name;
    @Column(name = "description", length = 45)
    private String description;
    @Basic(optional = false)
    @Column(name = "icon", nullable = false, length = 100)
    private String icon;
    @JoinColumn(name = "admin_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Admin adminId;

    public ItemsList() {
    }

    public ItemsList(Integer id) {
        this.id = id;
    }

    public ItemsList(Integer id, String icon) {
        this.id = id;
        this.icon = icon;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Admin getAdminId() {
        return adminId;
    }

    public void setAdminId(Admin adminId) {
        this.adminId = adminId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemsList)) {
            return false;
        }
        ItemsList other = (ItemsList) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "interfaces.ItemsList[ id=" + id + " ]";
    }
    
}
