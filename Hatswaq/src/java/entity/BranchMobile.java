/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Aya
 */
@Entity
@Table(name = "branch_mobile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BranchMobile.findAll", query = "SELECT b FROM BranchMobile b"),
    @NamedQuery(name = "BranchMobile.findById", query = "SELECT b FROM BranchMobile b WHERE b.id = :id"),
    @NamedQuery(name = "BranchMobile.findByMobile", query = "SELECT b FROM BranchMobile b WHERE b.mobile = :mobile")})
public class BranchMobile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "mobile", nullable = false, length = 20)
    private String mobile;
    @JoinColumn(name = "branche_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Branch brancheId;

    public BranchMobile() {
    }

    public BranchMobile(Integer id) {
        this.id = id;
    }

    public BranchMobile(Integer id, String mobile) {
        this.id = id;
        this.mobile = mobile;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Branch getBrancheId() {
        return brancheId;
    }

    public void setBrancheId(Branch brancheId) {
        this.brancheId = brancheId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BranchMobile)) {
            return false;
        }
        BranchMobile other = (BranchMobile) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "interfaces.BranchMobile[ id=" + id + " ]";
    }
    
}
