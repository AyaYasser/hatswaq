/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Aya
 */
@Entity
@Table(name = "vendor_mobile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VendorMobile.findAll", query = "SELECT v FROM VendorMobile v"),
    @NamedQuery(name = "VendorMobile.findById", query = "SELECT v FROM VendorMobile v WHERE v.id = :id"),
    @NamedQuery(name = "VendorMobile.findByPhoneForAdmin", query = "SELECT v FROM VendorMobile v WHERE v.phoneForAdmin = :phoneForAdmin"),
    @NamedQuery(name = "VendorMobile.findByPhoneForUser", query = "SELECT v FROM VendorMobile v WHERE v.phoneForUser = :phoneForUser"),
    @NamedQuery(name = "VendorMobile.findByStatus", query = "SELECT v FROM VendorMobile v WHERE v.status = :status")})
public class VendorMobile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "phone_for_admin", nullable = false, length = 20)
    private String phoneForAdmin;
    @Column(name = "phone_for_user", length = 20)
    private String phoneForUser;
    @Basic(optional = false)
    @Column(name = "status", nullable = false)
    private boolean status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vendorMobile")
    private List<Vendor> vendorList;

    public VendorMobile() {
    }

    public VendorMobile(Integer id) {
        this.id = id;
    }

    public VendorMobile(Integer id, String phoneForAdmin, boolean status) {
        this.id = id;
        this.phoneForAdmin = phoneForAdmin;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhoneForAdmin() {
        return phoneForAdmin;
    }

    public void setPhoneForAdmin(String phoneForAdmin) {
        this.phoneForAdmin = phoneForAdmin;
    }

    public String getPhoneForUser() {
        return phoneForUser;
    }

    public void setPhoneForUser(String phoneForUser) {
        this.phoneForUser = phoneForUser;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @XmlTransient
    public List<Vendor> getVendorList() {
        return vendorList;
    }

    public void setVendorList(List<Vendor> vendorList) {
        this.vendorList = vendorList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VendorMobile)) {
            return false;
        }
        VendorMobile other = (VendorMobile) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "interfaces.VendorMobile[ id=" + id + " ]";
    }
    
}
