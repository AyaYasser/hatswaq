/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Aya
 */
@Entity
@Table(name = "admin_communicate_vendor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AdminCommunicateVendor.findAll", query = "SELECT a FROM AdminCommunicateVendor a"),
    @NamedQuery(name = "AdminCommunicateVendor.findById", query = "SELECT a FROM AdminCommunicateVendor a WHERE a.id = :id"),
    @NamedQuery(name = "AdminCommunicateVendor.findByCommunitcateType", query = "SELECT a FROM AdminCommunicateVendor a WHERE a.communitcateType = :communitcateType"),
    @NamedQuery(name = "AdminCommunicateVendor.findByDirection", query = "SELECT a FROM AdminCommunicateVendor a WHERE a.direction = :direction")})
public class AdminCommunicateVendor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "communitcate_type", nullable = false)
    private boolean communitcateType;
    @Basic(optional = false)
    @Column(name = "direction", nullable = false)
    private boolean direction;
    @JoinColumn(name = "message_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Message messageId;
    @JoinColumn(name = "admin_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Admin adminId;
    @JoinColumn(name = "vendor_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Vendor vendorId;

    public AdminCommunicateVendor() {
    }

    public AdminCommunicateVendor(Integer id) {
        this.id = id;
    }

    public AdminCommunicateVendor(Integer id, boolean communitcateType, boolean direction) {
        this.id = id;
        this.communitcateType = communitcateType;
        this.direction = direction;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean getCommunitcateType() {
        return communitcateType;
    }

    public void setCommunitcateType(boolean communitcateType) {
        this.communitcateType = communitcateType;
    }

    public boolean getDirection() {
        return direction;
    }

    public void setDirection(boolean direction) {
        this.direction = direction;
    }

    public Message getMessageId() {
        return messageId;
    }

    public void setMessageId(Message messageId) {
        this.messageId = messageId;
    }

    public Admin getAdminId() {
        return adminId;
    }

    public void setAdminId(Admin adminId) {
        this.adminId = adminId;
    }

    public Vendor getVendorId() {
        return vendorId;
    }

    public void setVendorId(Vendor vendorId) {
        this.vendorId = vendorId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminCommunicateVendor)) {
            return false;
        }
        AdminCommunicateVendor other = (AdminCommunicateVendor) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "interfaces.AdminCommunicateVendor[ id=" + id + " ]";
    }
    
}
