/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.dao;

import entity.Branch;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Vendor;
import entity.BranchMobile;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.dao.exceptions.IllegalOrphanException;
import jpa.dao.exceptions.NonexistentEntityException;
import jpa.dao.exceptions.PreexistingEntityException;

/**
 *
 * @author Aya
 */
public class BranchJpaController implements Serializable {

    public BranchJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Branch branch) throws PreexistingEntityException, Exception {
        if (branch.getBranchMobileList() == null) {
            branch.setBranchMobileList(new ArrayList<BranchMobile>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Vendor vendorId = branch.getVendorId();
            if (vendorId != null) {
                vendorId = em.getReference(vendorId.getClass(), vendorId.getVendorPK());
                branch.setVendorId(vendorId);
            }
            List<BranchMobile> attachedBranchMobileList = new ArrayList<BranchMobile>();
            for (BranchMobile branchMobileListBranchMobileToAttach : branch.getBranchMobileList()) {
                branchMobileListBranchMobileToAttach = em.getReference(branchMobileListBranchMobileToAttach.getClass(), branchMobileListBranchMobileToAttach.getId());
                attachedBranchMobileList.add(branchMobileListBranchMobileToAttach);
            }
            branch.setBranchMobileList(attachedBranchMobileList);
            em.persist(branch);
            if (vendorId != null) {
                vendorId.getBranchList().add(branch);
                vendorId = em.merge(vendorId);
            }
            for (BranchMobile branchMobileListBranchMobile : branch.getBranchMobileList()) {
                Branch oldBrancheIdOfBranchMobileListBranchMobile = branchMobileListBranchMobile.getBrancheId();
                branchMobileListBranchMobile.setBrancheId(branch);
                branchMobileListBranchMobile = em.merge(branchMobileListBranchMobile);
                if (oldBrancheIdOfBranchMobileListBranchMobile != null) {
                    oldBrancheIdOfBranchMobileListBranchMobile.getBranchMobileList().remove(branchMobileListBranchMobile);
                    oldBrancheIdOfBranchMobileListBranchMobile = em.merge(oldBrancheIdOfBranchMobileListBranchMobile);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findBranch(branch.getId()) != null) {
                throw new PreexistingEntityException("Branch " + branch + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Branch branch) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Branch persistentBranch = em.find(Branch.class, branch.getId());
            Vendor vendorIdOld = persistentBranch.getVendorId();
            Vendor vendorIdNew = branch.getVendorId();
            List<BranchMobile> branchMobileListOld = persistentBranch.getBranchMobileList();
            List<BranchMobile> branchMobileListNew = branch.getBranchMobileList();
            List<String> illegalOrphanMessages = null;
            for (BranchMobile branchMobileListOldBranchMobile : branchMobileListOld) {
                if (!branchMobileListNew.contains(branchMobileListOldBranchMobile)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain BranchMobile " + branchMobileListOldBranchMobile + " since its brancheId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (vendorIdNew != null) {
                vendorIdNew = em.getReference(vendorIdNew.getClass(), vendorIdNew.getVendorPK());
                branch.setVendorId(vendorIdNew);
            }
            List<BranchMobile> attachedBranchMobileListNew = new ArrayList<BranchMobile>();
            for (BranchMobile branchMobileListNewBranchMobileToAttach : branchMobileListNew) {
                branchMobileListNewBranchMobileToAttach = em.getReference(branchMobileListNewBranchMobileToAttach.getClass(), branchMobileListNewBranchMobileToAttach.getId());
                attachedBranchMobileListNew.add(branchMobileListNewBranchMobileToAttach);
            }
            branchMobileListNew = attachedBranchMobileListNew;
            branch.setBranchMobileList(branchMobileListNew);
            branch = em.merge(branch);
            if (vendorIdOld != null && !vendorIdOld.equals(vendorIdNew)) {
                vendorIdOld.getBranchList().remove(branch);
                vendorIdOld = em.merge(vendorIdOld);
            }
            if (vendorIdNew != null && !vendorIdNew.equals(vendorIdOld)) {
                vendorIdNew.getBranchList().add(branch);
                vendorIdNew = em.merge(vendorIdNew);
            }
            for (BranchMobile branchMobileListNewBranchMobile : branchMobileListNew) {
                if (!branchMobileListOld.contains(branchMobileListNewBranchMobile)) {
                    Branch oldBrancheIdOfBranchMobileListNewBranchMobile = branchMobileListNewBranchMobile.getBrancheId();
                    branchMobileListNewBranchMobile.setBrancheId(branch);
                    branchMobileListNewBranchMobile = em.merge(branchMobileListNewBranchMobile);
                    if (oldBrancheIdOfBranchMobileListNewBranchMobile != null && !oldBrancheIdOfBranchMobileListNewBranchMobile.equals(branch)) {
                        oldBrancheIdOfBranchMobileListNewBranchMobile.getBranchMobileList().remove(branchMobileListNewBranchMobile);
                        oldBrancheIdOfBranchMobileListNewBranchMobile = em.merge(oldBrancheIdOfBranchMobileListNewBranchMobile);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = branch.getId();
                if (findBranch(id) == null) {
                    throw new NonexistentEntityException("The branch with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Branch branch;
            try {
                branch = em.getReference(Branch.class, id);
                branch.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The branch with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<BranchMobile> branchMobileListOrphanCheck = branch.getBranchMobileList();
            for (BranchMobile branchMobileListOrphanCheckBranchMobile : branchMobileListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Branch (" + branch + ") cannot be destroyed since the BranchMobile " + branchMobileListOrphanCheckBranchMobile + " in its branchMobileList field has a non-nullable brancheId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Vendor vendorId = branch.getVendorId();
            if (vendorId != null) {
                vendorId.getBranchList().remove(branch);
                vendorId = em.merge(vendorId);
            }
            em.remove(branch);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Branch> findBranchEntities() {
        return findBranchEntities(true, -1, -1);
    }

    public List<Branch> findBranchEntities(int maxResults, int firstResult) {
        return findBranchEntities(false, maxResults, firstResult);
    }

    private List<Branch> findBranchEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Branch.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Branch findBranch(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Branch.class, id);
        } finally {
            em.close();
        }
    }

    public int getBranchCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Branch> rt = cq.from(Branch.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
