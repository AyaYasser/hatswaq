/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.dao;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.VendorMobile;
import entity.Offer;
import java.util.ArrayList;
import java.util.List;
import entity.AdminCommunicateVendor;
import entity.Branch;
import entity.NotificationsHasVendor;
import entity.Vendor;
import entity.VendorPK;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.dao.exceptions.IllegalOrphanException;
import jpa.dao.exceptions.NonexistentEntityException;
import jpa.dao.exceptions.PreexistingEntityException;

/**
 *
 * @author Aya
 */
public class VendorJpaController implements Serializable {

    public VendorJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Vendor vendor) throws PreexistingEntityException, Exception {
        if (vendor.getVendorPK() == null) {
            vendor.setVendorPK(new VendorPK());
        }
        if (vendor.getOfferList() == null) {
            vendor.setOfferList(new ArrayList<Offer>());
        }
        if (vendor.getAdminCommunicateVendorList() == null) {
            vendor.setAdminCommunicateVendorList(new ArrayList<AdminCommunicateVendor>());
        }
        if (vendor.getBranchList() == null) {
            vendor.setBranchList(new ArrayList<Branch>());
        }
        if (vendor.getNotificationsHasVendorList() == null) {
            vendor.setNotificationsHasVendorList(new ArrayList<NotificationsHasVendor>());
        }
        vendor.getVendorPK().setVendorMobileId(vendor.getVendorMobile().getId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            VendorMobile vendorMobile = vendor.getVendorMobile();
            if (vendorMobile != null) {
                vendorMobile = em.getReference(vendorMobile.getClass(), vendorMobile.getId());
                vendor.setVendorMobile(vendorMobile);
            }
            List<Offer> attachedOfferList = new ArrayList<Offer>();
            for (Offer offerListOfferToAttach : vendor.getOfferList()) {
                offerListOfferToAttach = em.getReference(offerListOfferToAttach.getClass(), offerListOfferToAttach.getId());
                attachedOfferList.add(offerListOfferToAttach);
            }
            vendor.setOfferList(attachedOfferList);
            List<AdminCommunicateVendor> attachedAdminCommunicateVendorList = new ArrayList<AdminCommunicateVendor>();
            for (AdminCommunicateVendor adminCommunicateVendorListAdminCommunicateVendorToAttach : vendor.getAdminCommunicateVendorList()) {
                adminCommunicateVendorListAdminCommunicateVendorToAttach = em.getReference(adminCommunicateVendorListAdminCommunicateVendorToAttach.getClass(), adminCommunicateVendorListAdminCommunicateVendorToAttach.getId());
                attachedAdminCommunicateVendorList.add(adminCommunicateVendorListAdminCommunicateVendorToAttach);
            }
            vendor.setAdminCommunicateVendorList(attachedAdminCommunicateVendorList);
            List<Branch> attachedBranchList = new ArrayList<Branch>();
            for (Branch branchListBranchToAttach : vendor.getBranchList()) {
                branchListBranchToAttach = em.getReference(branchListBranchToAttach.getClass(), branchListBranchToAttach.getId());
                attachedBranchList.add(branchListBranchToAttach);
            }
            vendor.setBranchList(attachedBranchList);
            List<NotificationsHasVendor> attachedNotificationsHasVendorList = new ArrayList<NotificationsHasVendor>();
            for (NotificationsHasVendor notificationsHasVendorListNotificationsHasVendorToAttach : vendor.getNotificationsHasVendorList()) {
                notificationsHasVendorListNotificationsHasVendorToAttach = em.getReference(notificationsHasVendorListNotificationsHasVendorToAttach.getClass(), notificationsHasVendorListNotificationsHasVendorToAttach.getNotificationsHasVendorPK());
                attachedNotificationsHasVendorList.add(notificationsHasVendorListNotificationsHasVendorToAttach);
            }
            vendor.setNotificationsHasVendorList(attachedNotificationsHasVendorList);
            em.persist(vendor);
            if (vendorMobile != null) {
                vendorMobile.getVendorList().add(vendor);
                vendorMobile = em.merge(vendorMobile);
            }
            for (Offer offerListOffer : vendor.getOfferList()) {
                Vendor oldVendorIdOfOfferListOffer = offerListOffer.getVendorId();
                offerListOffer.setVendorId(vendor);
                offerListOffer = em.merge(offerListOffer);
                if (oldVendorIdOfOfferListOffer != null) {
                    oldVendorIdOfOfferListOffer.getOfferList().remove(offerListOffer);
                    oldVendorIdOfOfferListOffer = em.merge(oldVendorIdOfOfferListOffer);
                }
            }
            for (AdminCommunicateVendor adminCommunicateVendorListAdminCommunicateVendor : vendor.getAdminCommunicateVendorList()) {
                Vendor oldVendorIdOfAdminCommunicateVendorListAdminCommunicateVendor = adminCommunicateVendorListAdminCommunicateVendor.getVendorId();
                adminCommunicateVendorListAdminCommunicateVendor.setVendorId(vendor);
                adminCommunicateVendorListAdminCommunicateVendor = em.merge(adminCommunicateVendorListAdminCommunicateVendor);
                if (oldVendorIdOfAdminCommunicateVendorListAdminCommunicateVendor != null) {
                    oldVendorIdOfAdminCommunicateVendorListAdminCommunicateVendor.getAdminCommunicateVendorList().remove(adminCommunicateVendorListAdminCommunicateVendor);
                    oldVendorIdOfAdminCommunicateVendorListAdminCommunicateVendor = em.merge(oldVendorIdOfAdminCommunicateVendorListAdminCommunicateVendor);
                }
            }
            for (Branch branchListBranch : vendor.getBranchList()) {
                Vendor oldVendorIdOfBranchListBranch = branchListBranch.getVendorId();
                branchListBranch.setVendorId(vendor);
                branchListBranch = em.merge(branchListBranch);
                if (oldVendorIdOfBranchListBranch != null) {
                    oldVendorIdOfBranchListBranch.getBranchList().remove(branchListBranch);
                    oldVendorIdOfBranchListBranch = em.merge(oldVendorIdOfBranchListBranch);
                }
            }
            for (NotificationsHasVendor notificationsHasVendorListNotificationsHasVendor : vendor.getNotificationsHasVendorList()) {
                Vendor oldVendorOfNotificationsHasVendorListNotificationsHasVendor = notificationsHasVendorListNotificationsHasVendor.getVendor();
                notificationsHasVendorListNotificationsHasVendor.setVendor(vendor);
                notificationsHasVendorListNotificationsHasVendor = em.merge(notificationsHasVendorListNotificationsHasVendor);
                if (oldVendorOfNotificationsHasVendorListNotificationsHasVendor != null) {
                    oldVendorOfNotificationsHasVendorListNotificationsHasVendor.getNotificationsHasVendorList().remove(notificationsHasVendorListNotificationsHasVendor);
                    oldVendorOfNotificationsHasVendorListNotificationsHasVendor = em.merge(oldVendorOfNotificationsHasVendorListNotificationsHasVendor);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findVendor(vendor.getVendorPK()) != null) {
                throw new PreexistingEntityException("Vendor " + vendor + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Vendor vendor) throws IllegalOrphanException, NonexistentEntityException, Exception {
        vendor.getVendorPK().setVendorMobileId(vendor.getVendorMobile().getId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Vendor persistentVendor = em.find(Vendor.class, vendor.getVendorPK());
            VendorMobile vendorMobileOld = persistentVendor.getVendorMobile();
            VendorMobile vendorMobileNew = vendor.getVendorMobile();
            List<Offer> offerListOld = persistentVendor.getOfferList();
            List<Offer> offerListNew = vendor.getOfferList();
            List<AdminCommunicateVendor> adminCommunicateVendorListOld = persistentVendor.getAdminCommunicateVendorList();
            List<AdminCommunicateVendor> adminCommunicateVendorListNew = vendor.getAdminCommunicateVendorList();
            List<Branch> branchListOld = persistentVendor.getBranchList();
            List<Branch> branchListNew = vendor.getBranchList();
            List<NotificationsHasVendor> notificationsHasVendorListOld = persistentVendor.getNotificationsHasVendorList();
            List<NotificationsHasVendor> notificationsHasVendorListNew = vendor.getNotificationsHasVendorList();
            List<String> illegalOrphanMessages = null;
            for (Offer offerListOldOffer : offerListOld) {
                if (!offerListNew.contains(offerListOldOffer)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Offer " + offerListOldOffer + " since its vendorId field is not nullable.");
                }
            }
            for (AdminCommunicateVendor adminCommunicateVendorListOldAdminCommunicateVendor : adminCommunicateVendorListOld) {
                if (!adminCommunicateVendorListNew.contains(adminCommunicateVendorListOldAdminCommunicateVendor)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain AdminCommunicateVendor " + adminCommunicateVendorListOldAdminCommunicateVendor + " since its vendorId field is not nullable.");
                }
            }
            for (Branch branchListOldBranch : branchListOld) {
                if (!branchListNew.contains(branchListOldBranch)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Branch " + branchListOldBranch + " since its vendorId field is not nullable.");
                }
            }
            for (NotificationsHasVendor notificationsHasVendorListOldNotificationsHasVendor : notificationsHasVendorListOld) {
                if (!notificationsHasVendorListNew.contains(notificationsHasVendorListOldNotificationsHasVendor)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain NotificationsHasVendor " + notificationsHasVendorListOldNotificationsHasVendor + " since its vendor field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (vendorMobileNew != null) {
                vendorMobileNew = em.getReference(vendorMobileNew.getClass(), vendorMobileNew.getId());
                vendor.setVendorMobile(vendorMobileNew);
            }
            List<Offer> attachedOfferListNew = new ArrayList<Offer>();
            for (Offer offerListNewOfferToAttach : offerListNew) {
                offerListNewOfferToAttach = em.getReference(offerListNewOfferToAttach.getClass(), offerListNewOfferToAttach.getId());
                attachedOfferListNew.add(offerListNewOfferToAttach);
            }
            offerListNew = attachedOfferListNew;
            vendor.setOfferList(offerListNew);
            List<AdminCommunicateVendor> attachedAdminCommunicateVendorListNew = new ArrayList<AdminCommunicateVendor>();
            for (AdminCommunicateVendor adminCommunicateVendorListNewAdminCommunicateVendorToAttach : adminCommunicateVendorListNew) {
                adminCommunicateVendorListNewAdminCommunicateVendorToAttach = em.getReference(adminCommunicateVendorListNewAdminCommunicateVendorToAttach.getClass(), adminCommunicateVendorListNewAdminCommunicateVendorToAttach.getId());
                attachedAdminCommunicateVendorListNew.add(adminCommunicateVendorListNewAdminCommunicateVendorToAttach);
            }
            adminCommunicateVendorListNew = attachedAdminCommunicateVendorListNew;
            vendor.setAdminCommunicateVendorList(adminCommunicateVendorListNew);
            List<Branch> attachedBranchListNew = new ArrayList<Branch>();
            for (Branch branchListNewBranchToAttach : branchListNew) {
                branchListNewBranchToAttach = em.getReference(branchListNewBranchToAttach.getClass(), branchListNewBranchToAttach.getId());
                attachedBranchListNew.add(branchListNewBranchToAttach);
            }
            branchListNew = attachedBranchListNew;
            vendor.setBranchList(branchListNew);
            List<NotificationsHasVendor> attachedNotificationsHasVendorListNew = new ArrayList<NotificationsHasVendor>();
            for (NotificationsHasVendor notificationsHasVendorListNewNotificationsHasVendorToAttach : notificationsHasVendorListNew) {
                notificationsHasVendorListNewNotificationsHasVendorToAttach = em.getReference(notificationsHasVendorListNewNotificationsHasVendorToAttach.getClass(), notificationsHasVendorListNewNotificationsHasVendorToAttach.getNotificationsHasVendorPK());
                attachedNotificationsHasVendorListNew.add(notificationsHasVendorListNewNotificationsHasVendorToAttach);
            }
            notificationsHasVendorListNew = attachedNotificationsHasVendorListNew;
            vendor.setNotificationsHasVendorList(notificationsHasVendorListNew);
            vendor = em.merge(vendor);
            if (vendorMobileOld != null && !vendorMobileOld.equals(vendorMobileNew)) {
                vendorMobileOld.getVendorList().remove(vendor);
                vendorMobileOld = em.merge(vendorMobileOld);
            }
            if (vendorMobileNew != null && !vendorMobileNew.equals(vendorMobileOld)) {
                vendorMobileNew.getVendorList().add(vendor);
                vendorMobileNew = em.merge(vendorMobileNew);
            }
            for (Offer offerListNewOffer : offerListNew) {
                if (!offerListOld.contains(offerListNewOffer)) {
                    Vendor oldVendorIdOfOfferListNewOffer = offerListNewOffer.getVendorId();
                    offerListNewOffer.setVendorId(vendor);
                    offerListNewOffer = em.merge(offerListNewOffer);
                    if (oldVendorIdOfOfferListNewOffer != null && !oldVendorIdOfOfferListNewOffer.equals(vendor)) {
                        oldVendorIdOfOfferListNewOffer.getOfferList().remove(offerListNewOffer);
                        oldVendorIdOfOfferListNewOffer = em.merge(oldVendorIdOfOfferListNewOffer);
                    }
                }
            }
            for (AdminCommunicateVendor adminCommunicateVendorListNewAdminCommunicateVendor : adminCommunicateVendorListNew) {
                if (!adminCommunicateVendorListOld.contains(adminCommunicateVendorListNewAdminCommunicateVendor)) {
                    Vendor oldVendorIdOfAdminCommunicateVendorListNewAdminCommunicateVendor = adminCommunicateVendorListNewAdminCommunicateVendor.getVendorId();
                    adminCommunicateVendorListNewAdminCommunicateVendor.setVendorId(vendor);
                    adminCommunicateVendorListNewAdminCommunicateVendor = em.merge(adminCommunicateVendorListNewAdminCommunicateVendor);
                    if (oldVendorIdOfAdminCommunicateVendorListNewAdminCommunicateVendor != null && !oldVendorIdOfAdminCommunicateVendorListNewAdminCommunicateVendor.equals(vendor)) {
                        oldVendorIdOfAdminCommunicateVendorListNewAdminCommunicateVendor.getAdminCommunicateVendorList().remove(adminCommunicateVendorListNewAdminCommunicateVendor);
                        oldVendorIdOfAdminCommunicateVendorListNewAdminCommunicateVendor = em.merge(oldVendorIdOfAdminCommunicateVendorListNewAdminCommunicateVendor);
                    }
                }
            }
            for (Branch branchListNewBranch : branchListNew) {
                if (!branchListOld.contains(branchListNewBranch)) {
                    Vendor oldVendorIdOfBranchListNewBranch = branchListNewBranch.getVendorId();
                    branchListNewBranch.setVendorId(vendor);
                    branchListNewBranch = em.merge(branchListNewBranch);
                    if (oldVendorIdOfBranchListNewBranch != null && !oldVendorIdOfBranchListNewBranch.equals(vendor)) {
                        oldVendorIdOfBranchListNewBranch.getBranchList().remove(branchListNewBranch);
                        oldVendorIdOfBranchListNewBranch = em.merge(oldVendorIdOfBranchListNewBranch);
                    }
                }
            }
            for (NotificationsHasVendor notificationsHasVendorListNewNotificationsHasVendor : notificationsHasVendorListNew) {
                if (!notificationsHasVendorListOld.contains(notificationsHasVendorListNewNotificationsHasVendor)) {
                    Vendor oldVendorOfNotificationsHasVendorListNewNotificationsHasVendor = notificationsHasVendorListNewNotificationsHasVendor.getVendor();
                    notificationsHasVendorListNewNotificationsHasVendor.setVendor(vendor);
                    notificationsHasVendorListNewNotificationsHasVendor = em.merge(notificationsHasVendorListNewNotificationsHasVendor);
                    if (oldVendorOfNotificationsHasVendorListNewNotificationsHasVendor != null && !oldVendorOfNotificationsHasVendorListNewNotificationsHasVendor.equals(vendor)) {
                        oldVendorOfNotificationsHasVendorListNewNotificationsHasVendor.getNotificationsHasVendorList().remove(notificationsHasVendorListNewNotificationsHasVendor);
                        oldVendorOfNotificationsHasVendorListNewNotificationsHasVendor = em.merge(oldVendorOfNotificationsHasVendorListNewNotificationsHasVendor);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                VendorPK id = vendor.getVendorPK();
                if (findVendor(id) == null) {
                    throw new NonexistentEntityException("The vendor with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(VendorPK id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Vendor vendor;
            try {
                vendor = em.getReference(Vendor.class, id);
                vendor.getVendorPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The vendor with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Offer> offerListOrphanCheck = vendor.getOfferList();
            for (Offer offerListOrphanCheckOffer : offerListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Vendor (" + vendor + ") cannot be destroyed since the Offer " + offerListOrphanCheckOffer + " in its offerList field has a non-nullable vendorId field.");
            }
            List<AdminCommunicateVendor> adminCommunicateVendorListOrphanCheck = vendor.getAdminCommunicateVendorList();
            for (AdminCommunicateVendor adminCommunicateVendorListOrphanCheckAdminCommunicateVendor : adminCommunicateVendorListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Vendor (" + vendor + ") cannot be destroyed since the AdminCommunicateVendor " + adminCommunicateVendorListOrphanCheckAdminCommunicateVendor + " in its adminCommunicateVendorList field has a non-nullable vendorId field.");
            }
            List<Branch> branchListOrphanCheck = vendor.getBranchList();
            for (Branch branchListOrphanCheckBranch : branchListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Vendor (" + vendor + ") cannot be destroyed since the Branch " + branchListOrphanCheckBranch + " in its branchList field has a non-nullable vendorId field.");
            }
            List<NotificationsHasVendor> notificationsHasVendorListOrphanCheck = vendor.getNotificationsHasVendorList();
            for (NotificationsHasVendor notificationsHasVendorListOrphanCheckNotificationsHasVendor : notificationsHasVendorListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Vendor (" + vendor + ") cannot be destroyed since the NotificationsHasVendor " + notificationsHasVendorListOrphanCheckNotificationsHasVendor + " in its notificationsHasVendorList field has a non-nullable vendor field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            VendorMobile vendorMobile = vendor.getVendorMobile();
            if (vendorMobile != null) {
                vendorMobile.getVendorList().remove(vendor);
                vendorMobile = em.merge(vendorMobile);
            }
            em.remove(vendor);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Vendor> findVendorEntities() {
        return findVendorEntities(true, -1, -1);
    }

    public List<Vendor> findVendorEntities(int maxResults, int firstResult) {
        return findVendorEntities(false, maxResults, firstResult);
    }

    private List<Vendor> findVendorEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Vendor.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Vendor findVendor(VendorPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Vendor.class, id);
        } finally {
            em.close();
        }
    }

    public int getVendorCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Vendor> rt = cq.from(Vendor.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
