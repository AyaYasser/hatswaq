/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.dao;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Admin;
import entity.ItemsList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.dao.exceptions.NonexistentEntityException;
import jpa.dao.exceptions.PreexistingEntityException;

/**
 *
 * @author Aya
 */
public class ItemsListJpaController implements Serializable {

    public ItemsListJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ItemsList itemsList) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Admin adminId = itemsList.getAdminId();
            if (adminId != null) {
                adminId = em.getReference(adminId.getClass(), adminId.getId());
                itemsList.setAdminId(adminId);
            }
            em.persist(itemsList);
            if (adminId != null) {
                adminId.getItemsListList().add(itemsList);
                adminId = em.merge(adminId);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findItemsList(itemsList.getId()) != null) {
                throw new PreexistingEntityException("ItemsList " + itemsList + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ItemsList itemsList) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ItemsList persistentItemsList = em.find(ItemsList.class, itemsList.getId());
            Admin adminIdOld = persistentItemsList.getAdminId();
            Admin adminIdNew = itemsList.getAdminId();
            if (adminIdNew != null) {
                adminIdNew = em.getReference(adminIdNew.getClass(), adminIdNew.getId());
                itemsList.setAdminId(adminIdNew);
            }
            itemsList = em.merge(itemsList);
            if (adminIdOld != null && !adminIdOld.equals(adminIdNew)) {
                adminIdOld.getItemsListList().remove(itemsList);
                adminIdOld = em.merge(adminIdOld);
            }
            if (adminIdNew != null && !adminIdNew.equals(adminIdOld)) {
                adminIdNew.getItemsListList().add(itemsList);
                adminIdNew = em.merge(adminIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = itemsList.getId();
                if (findItemsList(id) == null) {
                    throw new NonexistentEntityException("The itemsList with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ItemsList itemsList;
            try {
                itemsList = em.getReference(ItemsList.class, id);
                itemsList.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The itemsList with id " + id + " no longer exists.", enfe);
            }
            Admin adminId = itemsList.getAdminId();
            if (adminId != null) {
                adminId.getItemsListList().remove(itemsList);
                adminId = em.merge(adminId);
            }
            em.remove(itemsList);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ItemsList> findItemsListEntities() {
        return findItemsListEntities(true, -1, -1);
    }

    public List<ItemsList> findItemsListEntities(int maxResults, int firstResult) {
        return findItemsListEntities(false, maxResults, firstResult);
    }

    private List<ItemsList> findItemsListEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ItemsList.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ItemsList findItemsList(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ItemsList.class, id);
        } finally {
            em.close();
        }
    }

    public int getItemsListCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ItemsList> rt = cq.from(ItemsList.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
