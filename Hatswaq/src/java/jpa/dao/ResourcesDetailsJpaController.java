/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.dao;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Item;
import entity.ResourcesDetails;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.dao.exceptions.NonexistentEntityException;
import jpa.dao.exceptions.PreexistingEntityException;

/**
 *
 * @author Aya
 */
public class ResourcesDetailsJpaController implements Serializable {

    public ResourcesDetailsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ResourcesDetails resourcesDetails) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Item itemId = resourcesDetails.getItemId();
            if (itemId != null) {
                itemId = em.getReference(itemId.getClass(), itemId.getItemPK());
                resourcesDetails.setItemId(itemId);
            }
            em.persist(resourcesDetails);
            if (itemId != null) {
                itemId.getResourcesDetailsList().add(resourcesDetails);
                itemId = em.merge(itemId);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findResourcesDetails(resourcesDetails.getId()) != null) {
                throw new PreexistingEntityException("ResourcesDetails " + resourcesDetails + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ResourcesDetails resourcesDetails) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ResourcesDetails persistentResourcesDetails = em.find(ResourcesDetails.class, resourcesDetails.getId());
            Item itemIdOld = persistentResourcesDetails.getItemId();
            Item itemIdNew = resourcesDetails.getItemId();
            if (itemIdNew != null) {
                itemIdNew = em.getReference(itemIdNew.getClass(), itemIdNew.getItemPK());
                resourcesDetails.setItemId(itemIdNew);
            }
            resourcesDetails = em.merge(resourcesDetails);
            if (itemIdOld != null && !itemIdOld.equals(itemIdNew)) {
                itemIdOld.getResourcesDetailsList().remove(resourcesDetails);
                itemIdOld = em.merge(itemIdOld);
            }
            if (itemIdNew != null && !itemIdNew.equals(itemIdOld)) {
                itemIdNew.getResourcesDetailsList().add(resourcesDetails);
                itemIdNew = em.merge(itemIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = resourcesDetails.getId();
                if (findResourcesDetails(id) == null) {
                    throw new NonexistentEntityException("The resourcesDetails with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ResourcesDetails resourcesDetails;
            try {
                resourcesDetails = em.getReference(ResourcesDetails.class, id);
                resourcesDetails.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The resourcesDetails with id " + id + " no longer exists.", enfe);
            }
            Item itemId = resourcesDetails.getItemId();
            if (itemId != null) {
                itemId.getResourcesDetailsList().remove(resourcesDetails);
                itemId = em.merge(itemId);
            }
            em.remove(resourcesDetails);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ResourcesDetails> findResourcesDetailsEntities() {
        return findResourcesDetailsEntities(true, -1, -1);
    }

    public List<ResourcesDetails> findResourcesDetailsEntities(int maxResults, int firstResult) {
        return findResourcesDetailsEntities(false, maxResults, firstResult);
    }

    private List<ResourcesDetails> findResourcesDetailsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ResourcesDetails.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ResourcesDetails findResourcesDetails(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ResourcesDetails.class, id);
        } finally {
            em.close();
        }
    }

    public int getResourcesDetailsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ResourcesDetails> rt = cq.from(ResourcesDetails.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
