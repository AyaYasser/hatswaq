/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.dao;

import entity.Admin;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.AdminCommunicateVendor;
import java.util.ArrayList;
import java.util.List;
import entity.ItemsList;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.dao.exceptions.IllegalOrphanException;
import jpa.dao.exceptions.NonexistentEntityException;
import jpa.dao.exceptions.PreexistingEntityException;

/**
 *
 * @author Aya
 */
public class AdminJpaController implements Serializable {

    public AdminJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Admin admin) throws PreexistingEntityException, Exception {
        if (admin.getAdminCommunicateVendorList() == null) {
            admin.setAdminCommunicateVendorList(new ArrayList<AdminCommunicateVendor>());
        }
        if (admin.getItemsListList() == null) {
            admin.setItemsListList(new ArrayList<ItemsList>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<AdminCommunicateVendor> attachedAdminCommunicateVendorList = new ArrayList<AdminCommunicateVendor>();
            for (AdminCommunicateVendor adminCommunicateVendorListAdminCommunicateVendorToAttach : admin.getAdminCommunicateVendorList()) {
                adminCommunicateVendorListAdminCommunicateVendorToAttach = em.getReference(adminCommunicateVendorListAdminCommunicateVendorToAttach.getClass(), adminCommunicateVendorListAdminCommunicateVendorToAttach.getId());
                attachedAdminCommunicateVendorList.add(adminCommunicateVendorListAdminCommunicateVendorToAttach);
            }
            admin.setAdminCommunicateVendorList(attachedAdminCommunicateVendorList);
            List<ItemsList> attachedItemsListList = new ArrayList<ItemsList>();
            for (ItemsList itemsListListItemsListToAttach : admin.getItemsListList()) {
                itemsListListItemsListToAttach = em.getReference(itemsListListItemsListToAttach.getClass(), itemsListListItemsListToAttach.getId());
                attachedItemsListList.add(itemsListListItemsListToAttach);
            }
            admin.setItemsListList(attachedItemsListList);
            em.persist(admin);
            for (AdminCommunicateVendor adminCommunicateVendorListAdminCommunicateVendor : admin.getAdminCommunicateVendorList()) {
                Admin oldAdminIdOfAdminCommunicateVendorListAdminCommunicateVendor = adminCommunicateVendorListAdminCommunicateVendor.getAdminId();
                adminCommunicateVendorListAdminCommunicateVendor.setAdminId(admin);
                adminCommunicateVendorListAdminCommunicateVendor = em.merge(adminCommunicateVendorListAdminCommunicateVendor);
                if (oldAdminIdOfAdminCommunicateVendorListAdminCommunicateVendor != null) {
                    oldAdminIdOfAdminCommunicateVendorListAdminCommunicateVendor.getAdminCommunicateVendorList().remove(adminCommunicateVendorListAdminCommunicateVendor);
                    oldAdminIdOfAdminCommunicateVendorListAdminCommunicateVendor = em.merge(oldAdminIdOfAdminCommunicateVendorListAdminCommunicateVendor);
                }
            }
            for (ItemsList itemsListListItemsList : admin.getItemsListList()) {
                Admin oldAdminIdOfItemsListListItemsList = itemsListListItemsList.getAdminId();
                itemsListListItemsList.setAdminId(admin);
                itemsListListItemsList = em.merge(itemsListListItemsList);
                if (oldAdminIdOfItemsListListItemsList != null) {
                    oldAdminIdOfItemsListListItemsList.getItemsListList().remove(itemsListListItemsList);
                    oldAdminIdOfItemsListListItemsList = em.merge(oldAdminIdOfItemsListListItemsList);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findAdmin(admin.getId()) != null) {
                throw new PreexistingEntityException("Admin " + admin + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Admin admin) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Admin persistentAdmin = em.find(Admin.class, admin.getId());
            List<AdminCommunicateVendor> adminCommunicateVendorListOld = persistentAdmin.getAdminCommunicateVendorList();
            List<AdminCommunicateVendor> adminCommunicateVendorListNew = admin.getAdminCommunicateVendorList();
            List<ItemsList> itemsListListOld = persistentAdmin.getItemsListList();
            List<ItemsList> itemsListListNew = admin.getItemsListList();
            List<String> illegalOrphanMessages = null;
            for (AdminCommunicateVendor adminCommunicateVendorListOldAdminCommunicateVendor : adminCommunicateVendorListOld) {
                if (!adminCommunicateVendorListNew.contains(adminCommunicateVendorListOldAdminCommunicateVendor)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain AdminCommunicateVendor " + adminCommunicateVendorListOldAdminCommunicateVendor + " since its adminId field is not nullable.");
                }
            }
            for (ItemsList itemsListListOldItemsList : itemsListListOld) {
                if (!itemsListListNew.contains(itemsListListOldItemsList)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ItemsList " + itemsListListOldItemsList + " since its adminId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<AdminCommunicateVendor> attachedAdminCommunicateVendorListNew = new ArrayList<AdminCommunicateVendor>();
            for (AdminCommunicateVendor adminCommunicateVendorListNewAdminCommunicateVendorToAttach : adminCommunicateVendorListNew) {
                adminCommunicateVendorListNewAdminCommunicateVendorToAttach = em.getReference(adminCommunicateVendorListNewAdminCommunicateVendorToAttach.getClass(), adminCommunicateVendorListNewAdminCommunicateVendorToAttach.getId());
                attachedAdminCommunicateVendorListNew.add(adminCommunicateVendorListNewAdminCommunicateVendorToAttach);
            }
            adminCommunicateVendorListNew = attachedAdminCommunicateVendorListNew;
            admin.setAdminCommunicateVendorList(adminCommunicateVendorListNew);
            List<ItemsList> attachedItemsListListNew = new ArrayList<ItemsList>();
            for (ItemsList itemsListListNewItemsListToAttach : itemsListListNew) {
                itemsListListNewItemsListToAttach = em.getReference(itemsListListNewItemsListToAttach.getClass(), itemsListListNewItemsListToAttach.getId());
                attachedItemsListListNew.add(itemsListListNewItemsListToAttach);
            }
            itemsListListNew = attachedItemsListListNew;
            admin.setItemsListList(itemsListListNew);
            admin = em.merge(admin);
            for (AdminCommunicateVendor adminCommunicateVendorListNewAdminCommunicateVendor : adminCommunicateVendorListNew) {
                if (!adminCommunicateVendorListOld.contains(adminCommunicateVendorListNewAdminCommunicateVendor)) {
                    Admin oldAdminIdOfAdminCommunicateVendorListNewAdminCommunicateVendor = adminCommunicateVendorListNewAdminCommunicateVendor.getAdminId();
                    adminCommunicateVendorListNewAdminCommunicateVendor.setAdminId(admin);
                    adminCommunicateVendorListNewAdminCommunicateVendor = em.merge(adminCommunicateVendorListNewAdminCommunicateVendor);
                    if (oldAdminIdOfAdminCommunicateVendorListNewAdminCommunicateVendor != null && !oldAdminIdOfAdminCommunicateVendorListNewAdminCommunicateVendor.equals(admin)) {
                        oldAdminIdOfAdminCommunicateVendorListNewAdminCommunicateVendor.getAdminCommunicateVendorList().remove(adminCommunicateVendorListNewAdminCommunicateVendor);
                        oldAdminIdOfAdminCommunicateVendorListNewAdminCommunicateVendor = em.merge(oldAdminIdOfAdminCommunicateVendorListNewAdminCommunicateVendor);
                    }
                }
            }
            for (ItemsList itemsListListNewItemsList : itemsListListNew) {
                if (!itemsListListOld.contains(itemsListListNewItemsList)) {
                    Admin oldAdminIdOfItemsListListNewItemsList = itemsListListNewItemsList.getAdminId();
                    itemsListListNewItemsList.setAdminId(admin);
                    itemsListListNewItemsList = em.merge(itemsListListNewItemsList);
                    if (oldAdminIdOfItemsListListNewItemsList != null && !oldAdminIdOfItemsListListNewItemsList.equals(admin)) {
                        oldAdminIdOfItemsListListNewItemsList.getItemsListList().remove(itemsListListNewItemsList);
                        oldAdminIdOfItemsListListNewItemsList = em.merge(oldAdminIdOfItemsListListNewItemsList);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = admin.getId();
                if (findAdmin(id) == null) {
                    throw new NonexistentEntityException("The admin with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Admin admin;
            try {
                admin = em.getReference(Admin.class, id);
                admin.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The admin with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<AdminCommunicateVendor> adminCommunicateVendorListOrphanCheck = admin.getAdminCommunicateVendorList();
            for (AdminCommunicateVendor adminCommunicateVendorListOrphanCheckAdminCommunicateVendor : adminCommunicateVendorListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Admin (" + admin + ") cannot be destroyed since the AdminCommunicateVendor " + adminCommunicateVendorListOrphanCheckAdminCommunicateVendor + " in its adminCommunicateVendorList field has a non-nullable adminId field.");
            }
            List<ItemsList> itemsListListOrphanCheck = admin.getItemsListList();
            for (ItemsList itemsListListOrphanCheckItemsList : itemsListListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Admin (" + admin + ") cannot be destroyed since the ItemsList " + itemsListListOrphanCheckItemsList + " in its itemsListList field has a non-nullable adminId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(admin);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Admin> findAdminEntities() {
        return findAdminEntities(true, -1, -1);
    }

    public List<Admin> findAdminEntities(int maxResults, int firstResult) {
        return findAdminEntities(false, maxResults, firstResult);
    }

    private List<Admin> findAdminEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Admin.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Admin findAdmin(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Admin.class, id);
        } finally {
            em.close();
        }
    }

    public int getAdminCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Admin> rt = cq.from(Admin.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
