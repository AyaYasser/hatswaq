/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.dao;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Vendor;
import entity.VendorMobile;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.dao.exceptions.IllegalOrphanException;
import jpa.dao.exceptions.NonexistentEntityException;
import jpa.dao.exceptions.PreexistingEntityException;

/**
 *
 * @author Aya
 */
public class VendorMobileJpaController implements Serializable {

    public VendorMobileJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(VendorMobile vendorMobile) throws PreexistingEntityException, Exception {
        if (vendorMobile.getVendorList() == null) {
            vendorMobile.setVendorList(new ArrayList<Vendor>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Vendor> attachedVendorList = new ArrayList<Vendor>();
            for (Vendor vendorListVendorToAttach : vendorMobile.getVendorList()) {
                vendorListVendorToAttach = em.getReference(vendorListVendorToAttach.getClass(), vendorListVendorToAttach.getVendorPK());
                attachedVendorList.add(vendorListVendorToAttach);
            }
            vendorMobile.setVendorList(attachedVendorList);
            em.persist(vendorMobile);
            for (Vendor vendorListVendor : vendorMobile.getVendorList()) {
                VendorMobile oldVendorMobileOfVendorListVendor = vendorListVendor.getVendorMobile();
                vendorListVendor.setVendorMobile(vendorMobile);
                vendorListVendor = em.merge(vendorListVendor);
                if (oldVendorMobileOfVendorListVendor != null) {
                    oldVendorMobileOfVendorListVendor.getVendorList().remove(vendorListVendor);
                    oldVendorMobileOfVendorListVendor = em.merge(oldVendorMobileOfVendorListVendor);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findVendorMobile(vendorMobile.getId()) != null) {
                throw new PreexistingEntityException("VendorMobile " + vendorMobile + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(VendorMobile vendorMobile) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            VendorMobile persistentVendorMobile = em.find(VendorMobile.class, vendorMobile.getId());
            List<Vendor> vendorListOld = persistentVendorMobile.getVendorList();
            List<Vendor> vendorListNew = vendorMobile.getVendorList();
            List<String> illegalOrphanMessages = null;
            for (Vendor vendorListOldVendor : vendorListOld) {
                if (!vendorListNew.contains(vendorListOldVendor)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Vendor " + vendorListOldVendor + " since its vendorMobile field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Vendor> attachedVendorListNew = new ArrayList<Vendor>();
            for (Vendor vendorListNewVendorToAttach : vendorListNew) {
                vendorListNewVendorToAttach = em.getReference(vendorListNewVendorToAttach.getClass(), vendorListNewVendorToAttach.getVendorPK());
                attachedVendorListNew.add(vendorListNewVendorToAttach);
            }
            vendorListNew = attachedVendorListNew;
            vendorMobile.setVendorList(vendorListNew);
            vendorMobile = em.merge(vendorMobile);
            for (Vendor vendorListNewVendor : vendorListNew) {
                if (!vendorListOld.contains(vendorListNewVendor)) {
                    VendorMobile oldVendorMobileOfVendorListNewVendor = vendorListNewVendor.getVendorMobile();
                    vendorListNewVendor.setVendorMobile(vendorMobile);
                    vendorListNewVendor = em.merge(vendorListNewVendor);
                    if (oldVendorMobileOfVendorListNewVendor != null && !oldVendorMobileOfVendorListNewVendor.equals(vendorMobile)) {
                        oldVendorMobileOfVendorListNewVendor.getVendorList().remove(vendorListNewVendor);
                        oldVendorMobileOfVendorListNewVendor = em.merge(oldVendorMobileOfVendorListNewVendor);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = vendorMobile.getId();
                if (findVendorMobile(id) == null) {
                    throw new NonexistentEntityException("The vendorMobile with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            VendorMobile vendorMobile;
            try {
                vendorMobile = em.getReference(VendorMobile.class, id);
                vendorMobile.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The vendorMobile with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Vendor> vendorListOrphanCheck = vendorMobile.getVendorList();
            for (Vendor vendorListOrphanCheckVendor : vendorListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This VendorMobile (" + vendorMobile + ") cannot be destroyed since the Vendor " + vendorListOrphanCheckVendor + " in its vendorList field has a non-nullable vendorMobile field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(vendorMobile);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<VendorMobile> findVendorMobileEntities() {
        return findVendorMobileEntities(true, -1, -1);
    }

    public List<VendorMobile> findVendorMobileEntities(int maxResults, int firstResult) {
        return findVendorMobileEntities(false, maxResults, firstResult);
    }

    private List<VendorMobile> findVendorMobileEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(VendorMobile.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public VendorMobile findVendorMobile(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(VendorMobile.class, id);
        } finally {
            em.close();
        }
    }

    public int getVendorMobileCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<VendorMobile> rt = cq.from(VendorMobile.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
