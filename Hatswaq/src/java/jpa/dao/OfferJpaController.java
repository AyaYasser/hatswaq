/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.dao;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Vendor;
import entity.Item;
import entity.Offer;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.dao.exceptions.IllegalOrphanException;
import jpa.dao.exceptions.NonexistentEntityException;
import jpa.dao.exceptions.PreexistingEntityException;

/**
 *
 * @author Aya
 */
public class OfferJpaController implements Serializable {

    public OfferJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Offer offer) throws PreexistingEntityException, Exception {
        if (offer.getItemList() == null) {
            offer.setItemList(new ArrayList<Item>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Vendor vendorId = offer.getVendorId();
            if (vendorId != null) {
                vendorId = em.getReference(vendorId.getClass(), vendorId.getVendorPK());
                offer.setVendorId(vendorId);
            }
            List<Item> attachedItemList = new ArrayList<Item>();
            for (Item itemListItemToAttach : offer.getItemList()) {
                itemListItemToAttach = em.getReference(itemListItemToAttach.getClass(), itemListItemToAttach.getItemPK());
                attachedItemList.add(itemListItemToAttach);
            }
            offer.setItemList(attachedItemList);
            em.persist(offer);
            if (vendorId != null) {
                vendorId.getOfferList().add(offer);
                vendorId = em.merge(vendorId);
            }
            for (Item itemListItem : offer.getItemList()) {
                Offer oldOfferOfItemListItem = itemListItem.getOffer();
                itemListItem.setOffer(offer);
                itemListItem = em.merge(itemListItem);
                if (oldOfferOfItemListItem != null) {
                    oldOfferOfItemListItem.getItemList().remove(itemListItem);
                    oldOfferOfItemListItem = em.merge(oldOfferOfItemListItem);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findOffer(offer.getId()) != null) {
                throw new PreexistingEntityException("Offer " + offer + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Offer offer) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Offer persistentOffer = em.find(Offer.class, offer.getId());
            Vendor vendorIdOld = persistentOffer.getVendorId();
            Vendor vendorIdNew = offer.getVendorId();
            List<Item> itemListOld = persistentOffer.getItemList();
            List<Item> itemListNew = offer.getItemList();
            List<String> illegalOrphanMessages = null;
            for (Item itemListOldItem : itemListOld) {
                if (!itemListNew.contains(itemListOldItem)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Item " + itemListOldItem + " since its offer field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (vendorIdNew != null) {
                vendorIdNew = em.getReference(vendorIdNew.getClass(), vendorIdNew.getVendorPK());
                offer.setVendorId(vendorIdNew);
            }
            List<Item> attachedItemListNew = new ArrayList<Item>();
            for (Item itemListNewItemToAttach : itemListNew) {
                itemListNewItemToAttach = em.getReference(itemListNewItemToAttach.getClass(), itemListNewItemToAttach.getItemPK());
                attachedItemListNew.add(itemListNewItemToAttach);
            }
            itemListNew = attachedItemListNew;
            offer.setItemList(itemListNew);
            offer = em.merge(offer);
            if (vendorIdOld != null && !vendorIdOld.equals(vendorIdNew)) {
                vendorIdOld.getOfferList().remove(offer);
                vendorIdOld = em.merge(vendorIdOld);
            }
            if (vendorIdNew != null && !vendorIdNew.equals(vendorIdOld)) {
                vendorIdNew.getOfferList().add(offer);
                vendorIdNew = em.merge(vendorIdNew);
            }
            for (Item itemListNewItem : itemListNew) {
                if (!itemListOld.contains(itemListNewItem)) {
                    Offer oldOfferOfItemListNewItem = itemListNewItem.getOffer();
                    itemListNewItem.setOffer(offer);
                    itemListNewItem = em.merge(itemListNewItem);
                    if (oldOfferOfItemListNewItem != null && !oldOfferOfItemListNewItem.equals(offer)) {
                        oldOfferOfItemListNewItem.getItemList().remove(itemListNewItem);
                        oldOfferOfItemListNewItem = em.merge(oldOfferOfItemListNewItem);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = offer.getId();
                if (findOffer(id) == null) {
                    throw new NonexistentEntityException("The offer with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Offer offer;
            try {
                offer = em.getReference(Offer.class, id);
                offer.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The offer with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Item> itemListOrphanCheck = offer.getItemList();
            for (Item itemListOrphanCheckItem : itemListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Offer (" + offer + ") cannot be destroyed since the Item " + itemListOrphanCheckItem + " in its itemList field has a non-nullable offer field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Vendor vendorId = offer.getVendorId();
            if (vendorId != null) {
                vendorId.getOfferList().remove(offer);
                vendorId = em.merge(vendorId);
            }
            em.remove(offer);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Offer> findOfferEntities() {
        return findOfferEntities(true, -1, -1);
    }

    public List<Offer> findOfferEntities(int maxResults, int firstResult) {
        return findOfferEntities(false, maxResults, firstResult);
    }

    private List<Offer> findOfferEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Offer.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Offer findOffer(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Offer.class, id);
        } finally {
            em.close();
        }
    }

    public int getOfferCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Offer> rt = cq.from(Offer.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
