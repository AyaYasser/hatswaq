/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.dao;

import entity.NotificationsHasVendor;
import entity.NotificationsHasVendorPK;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Vendor;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.dao.exceptions.NonexistentEntityException;
import jpa.dao.exceptions.PreexistingEntityException;

/**
 *
 * @author Aya
 */
public class NotificationsHasVendorJpaController implements Serializable {

    public NotificationsHasVendorJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(NotificationsHasVendor notificationsHasVendor) throws PreexistingEntityException, Exception {
        if (notificationsHasVendor.getNotificationsHasVendorPK() == null) {
            notificationsHasVendor.setNotificationsHasVendorPK(new NotificationsHasVendorPK());
        }
        notificationsHasVendor.getNotificationsHasVendorPK().setVendorVendorid(notificationsHasVendor.getVendor().getVendorPK().getId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Vendor vendor = notificationsHasVendor.getVendor();
            if (vendor != null) {
                vendor = em.getReference(vendor.getClass(), vendor.getVendorPK());
                notificationsHasVendor.setVendor(vendor);
            }
            em.persist(notificationsHasVendor);
            if (vendor != null) {
                vendor.getNotificationsHasVendorList().add(notificationsHasVendor);
                vendor = em.merge(vendor);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findNotificationsHasVendor(notificationsHasVendor.getNotificationsHasVendorPK()) != null) {
                throw new PreexistingEntityException("NotificationsHasVendor " + notificationsHasVendor + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(NotificationsHasVendor notificationsHasVendor) throws NonexistentEntityException, Exception {
        notificationsHasVendor.getNotificationsHasVendorPK().setVendorVendorid(notificationsHasVendor.getVendor().getVendorPK().getId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            NotificationsHasVendor persistentNotificationsHasVendor = em.find(NotificationsHasVendor.class, notificationsHasVendor.getNotificationsHasVendorPK());
            Vendor vendorOld = persistentNotificationsHasVendor.getVendor();
            Vendor vendorNew = notificationsHasVendor.getVendor();
            if (vendorNew != null) {
                vendorNew = em.getReference(vendorNew.getClass(), vendorNew.getVendorPK());
                notificationsHasVendor.setVendor(vendorNew);
            }
            notificationsHasVendor = em.merge(notificationsHasVendor);
            if (vendorOld != null && !vendorOld.equals(vendorNew)) {
                vendorOld.getNotificationsHasVendorList().remove(notificationsHasVendor);
                vendorOld = em.merge(vendorOld);
            }
            if (vendorNew != null && !vendorNew.equals(vendorOld)) {
                vendorNew.getNotificationsHasVendorList().add(notificationsHasVendor);
                vendorNew = em.merge(vendorNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                NotificationsHasVendorPK id = notificationsHasVendor.getNotificationsHasVendorPK();
                if (findNotificationsHasVendor(id) == null) {
                    throw new NonexistentEntityException("The notificationsHasVendor with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(NotificationsHasVendorPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            NotificationsHasVendor notificationsHasVendor;
            try {
                notificationsHasVendor = em.getReference(NotificationsHasVendor.class, id);
                notificationsHasVendor.getNotificationsHasVendorPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The notificationsHasVendor with id " + id + " no longer exists.", enfe);
            }
            Vendor vendor = notificationsHasVendor.getVendor();
            if (vendor != null) {
                vendor.getNotificationsHasVendorList().remove(notificationsHasVendor);
                vendor = em.merge(vendor);
            }
            em.remove(notificationsHasVendor);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<NotificationsHasVendor> findNotificationsHasVendorEntities() {
        return findNotificationsHasVendorEntities(true, -1, -1);
    }

    public List<NotificationsHasVendor> findNotificationsHasVendorEntities(int maxResults, int firstResult) {
        return findNotificationsHasVendorEntities(false, maxResults, firstResult);
    }

    private List<NotificationsHasVendor> findNotificationsHasVendorEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(NotificationsHasVendor.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public NotificationsHasVendor findNotificationsHasVendor(NotificationsHasVendorPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(NotificationsHasVendor.class, id);
        } finally {
            em.close();
        }
    }

    public int getNotificationsHasVendorCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<NotificationsHasVendor> rt = cq.from(NotificationsHasVendor.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
