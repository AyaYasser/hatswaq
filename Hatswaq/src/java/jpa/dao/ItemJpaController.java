/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.dao;

import entity.Item;
import entity.ItemPK;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Offer;
import entity.ResourcesDetails;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.dao.exceptions.IllegalOrphanException;
import jpa.dao.exceptions.NonexistentEntityException;
import jpa.dao.exceptions.PreexistingEntityException;

/**
 *
 * @author Aya
 */
public class ItemJpaController implements Serializable {

    public ItemJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Item item) throws PreexistingEntityException, Exception {
        if (item.getItemPK() == null) {
            item.setItemPK(new ItemPK());
        }
        if (item.getResourcesDetailsList() == null) {
            item.setResourcesDetailsList(new ArrayList<ResourcesDetails>());
        }
        item.getItemPK().setOfferId(item.getOffer().getId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Offer offer = item.getOffer();
            if (offer != null) {
                offer = em.getReference(offer.getClass(), offer.getId());
                item.setOffer(offer);
            }
            List<ResourcesDetails> attachedResourcesDetailsList = new ArrayList<ResourcesDetails>();
            for (ResourcesDetails resourcesDetailsListResourcesDetailsToAttach : item.getResourcesDetailsList()) {
                resourcesDetailsListResourcesDetailsToAttach = em.getReference(resourcesDetailsListResourcesDetailsToAttach.getClass(), resourcesDetailsListResourcesDetailsToAttach.getId());
                attachedResourcesDetailsList.add(resourcesDetailsListResourcesDetailsToAttach);
            }
            item.setResourcesDetailsList(attachedResourcesDetailsList);
            em.persist(item);
            if (offer != null) {
                offer.getItemList().add(item);
                offer = em.merge(offer);
            }
            for (ResourcesDetails resourcesDetailsListResourcesDetails : item.getResourcesDetailsList()) {
                Item oldItemIdOfResourcesDetailsListResourcesDetails = resourcesDetailsListResourcesDetails.getItemId();
                resourcesDetailsListResourcesDetails.setItemId(item);
                resourcesDetailsListResourcesDetails = em.merge(resourcesDetailsListResourcesDetails);
                if (oldItemIdOfResourcesDetailsListResourcesDetails != null) {
                    oldItemIdOfResourcesDetailsListResourcesDetails.getResourcesDetailsList().remove(resourcesDetailsListResourcesDetails);
                    oldItemIdOfResourcesDetailsListResourcesDetails = em.merge(oldItemIdOfResourcesDetailsListResourcesDetails);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findItem(item.getItemPK()) != null) {
                throw new PreexistingEntityException("Item " + item + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Item item) throws IllegalOrphanException, NonexistentEntityException, Exception {
        item.getItemPK().setOfferId(item.getOffer().getId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Item persistentItem = em.find(Item.class, item.getItemPK());
            Offer offerOld = persistentItem.getOffer();
            Offer offerNew = item.getOffer();
            List<ResourcesDetails> resourcesDetailsListOld = persistentItem.getResourcesDetailsList();
            List<ResourcesDetails> resourcesDetailsListNew = item.getResourcesDetailsList();
            List<String> illegalOrphanMessages = null;
            for (ResourcesDetails resourcesDetailsListOldResourcesDetails : resourcesDetailsListOld) {
                if (!resourcesDetailsListNew.contains(resourcesDetailsListOldResourcesDetails)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ResourcesDetails " + resourcesDetailsListOldResourcesDetails + " since its itemId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (offerNew != null) {
                offerNew = em.getReference(offerNew.getClass(), offerNew.getId());
                item.setOffer(offerNew);
            }
            List<ResourcesDetails> attachedResourcesDetailsListNew = new ArrayList<ResourcesDetails>();
            for (ResourcesDetails resourcesDetailsListNewResourcesDetailsToAttach : resourcesDetailsListNew) {
                resourcesDetailsListNewResourcesDetailsToAttach = em.getReference(resourcesDetailsListNewResourcesDetailsToAttach.getClass(), resourcesDetailsListNewResourcesDetailsToAttach.getId());
                attachedResourcesDetailsListNew.add(resourcesDetailsListNewResourcesDetailsToAttach);
            }
            resourcesDetailsListNew = attachedResourcesDetailsListNew;
            item.setResourcesDetailsList(resourcesDetailsListNew);
            item = em.merge(item);
            if (offerOld != null && !offerOld.equals(offerNew)) {
                offerOld.getItemList().remove(item);
                offerOld = em.merge(offerOld);
            }
            if (offerNew != null && !offerNew.equals(offerOld)) {
                offerNew.getItemList().add(item);
                offerNew = em.merge(offerNew);
            }
            for (ResourcesDetails resourcesDetailsListNewResourcesDetails : resourcesDetailsListNew) {
                if (!resourcesDetailsListOld.contains(resourcesDetailsListNewResourcesDetails)) {
                    Item oldItemIdOfResourcesDetailsListNewResourcesDetails = resourcesDetailsListNewResourcesDetails.getItemId();
                    resourcesDetailsListNewResourcesDetails.setItemId(item);
                    resourcesDetailsListNewResourcesDetails = em.merge(resourcesDetailsListNewResourcesDetails);
                    if (oldItemIdOfResourcesDetailsListNewResourcesDetails != null && !oldItemIdOfResourcesDetailsListNewResourcesDetails.equals(item)) {
                        oldItemIdOfResourcesDetailsListNewResourcesDetails.getResourcesDetailsList().remove(resourcesDetailsListNewResourcesDetails);
                        oldItemIdOfResourcesDetailsListNewResourcesDetails = em.merge(oldItemIdOfResourcesDetailsListNewResourcesDetails);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                ItemPK id = item.getItemPK();
                if (findItem(id) == null) {
                    throw new NonexistentEntityException("The item with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(ItemPK id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Item item;
            try {
                item = em.getReference(Item.class, id);
                item.getItemPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The item with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<ResourcesDetails> resourcesDetailsListOrphanCheck = item.getResourcesDetailsList();
            for (ResourcesDetails resourcesDetailsListOrphanCheckResourcesDetails : resourcesDetailsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Item (" + item + ") cannot be destroyed since the ResourcesDetails " + resourcesDetailsListOrphanCheckResourcesDetails + " in its resourcesDetailsList field has a non-nullable itemId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Offer offer = item.getOffer();
            if (offer != null) {
                offer.getItemList().remove(item);
                offer = em.merge(offer);
            }
            em.remove(item);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Item> findItemEntities() {
        return findItemEntities(true, -1, -1);
    }

    public List<Item> findItemEntities(int maxResults, int firstResult) {
        return findItemEntities(false, maxResults, firstResult);
    }

    private List<Item> findItemEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Item.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Item findItem(ItemPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Item.class, id);
        } finally {
            em.close();
        }
    }

    public int getItemCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Item> rt = cq.from(Item.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
