/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.dao;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Message;
import entity.Admin;
import entity.AdminCommunicateVendor;
import entity.Vendor;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.dao.exceptions.NonexistentEntityException;

/**
 *
 * @author Aya
 */
public class AdminCommunicateVendorJpaController implements Serializable {

    public AdminCommunicateVendorJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(AdminCommunicateVendor adminCommunicateVendor) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Message messageId = adminCommunicateVendor.getMessageId();
            if (messageId != null) {
                messageId = em.getReference(messageId.getClass(), messageId.getId());
                adminCommunicateVendor.setMessageId(messageId);
            }
            Admin adminId = adminCommunicateVendor.getAdminId();
            if (adminId != null) {
                adminId = em.getReference(adminId.getClass(), adminId.getId());
                adminCommunicateVendor.setAdminId(adminId);
            }
            Vendor vendorId = adminCommunicateVendor.getVendorId();
            if (vendorId != null) {
                vendorId = em.getReference(vendorId.getClass(), vendorId.getVendorPK());
                adminCommunicateVendor.setVendorId(vendorId);
            }
            em.persist(adminCommunicateVendor);
            if (messageId != null) {
                messageId.getAdminCommunicateVendorList().add(adminCommunicateVendor);
                messageId = em.merge(messageId);
            }
            if (adminId != null) {
                adminId.getAdminCommunicateVendorList().add(adminCommunicateVendor);
                adminId = em.merge(adminId);
            }
            if (vendorId != null) {
                vendorId.getAdminCommunicateVendorList().add(adminCommunicateVendor);
                vendorId = em.merge(vendorId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(AdminCommunicateVendor adminCommunicateVendor) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            AdminCommunicateVendor persistentAdminCommunicateVendor = em.find(AdminCommunicateVendor.class, adminCommunicateVendor.getId());
            Message messageIdOld = persistentAdminCommunicateVendor.getMessageId();
            Message messageIdNew = adminCommunicateVendor.getMessageId();
            Admin adminIdOld = persistentAdminCommunicateVendor.getAdminId();
            Admin adminIdNew = adminCommunicateVendor.getAdminId();
            Vendor vendorIdOld = persistentAdminCommunicateVendor.getVendorId();
            Vendor vendorIdNew = adminCommunicateVendor.getVendorId();
            if (messageIdNew != null) {
                messageIdNew = em.getReference(messageIdNew.getClass(), messageIdNew.getId());
                adminCommunicateVendor.setMessageId(messageIdNew);
            }
            if (adminIdNew != null) {
                adminIdNew = em.getReference(adminIdNew.getClass(), adminIdNew.getId());
                adminCommunicateVendor.setAdminId(adminIdNew);
            }
            if (vendorIdNew != null) {
                vendorIdNew = em.getReference(vendorIdNew.getClass(), vendorIdNew.getVendorPK());
                adminCommunicateVendor.setVendorId(vendorIdNew);
            }
            adminCommunicateVendor = em.merge(adminCommunicateVendor);
            if (messageIdOld != null && !messageIdOld.equals(messageIdNew)) {
                messageIdOld.getAdminCommunicateVendorList().remove(adminCommunicateVendor);
                messageIdOld = em.merge(messageIdOld);
            }
            if (messageIdNew != null && !messageIdNew.equals(messageIdOld)) {
                messageIdNew.getAdminCommunicateVendorList().add(adminCommunicateVendor);
                messageIdNew = em.merge(messageIdNew);
            }
            if (adminIdOld != null && !adminIdOld.equals(adminIdNew)) {
                adminIdOld.getAdminCommunicateVendorList().remove(adminCommunicateVendor);
                adminIdOld = em.merge(adminIdOld);
            }
            if (adminIdNew != null && !adminIdNew.equals(adminIdOld)) {
                adminIdNew.getAdminCommunicateVendorList().add(adminCommunicateVendor);
                adminIdNew = em.merge(adminIdNew);
            }
            if (vendorIdOld != null && !vendorIdOld.equals(vendorIdNew)) {
                vendorIdOld.getAdminCommunicateVendorList().remove(adminCommunicateVendor);
                vendorIdOld = em.merge(vendorIdOld);
            }
            if (vendorIdNew != null && !vendorIdNew.equals(vendorIdOld)) {
                vendorIdNew.getAdminCommunicateVendorList().add(adminCommunicateVendor);
                vendorIdNew = em.merge(vendorIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = adminCommunicateVendor.getId();
                if (findAdminCommunicateVendor(id) == null) {
                    throw new NonexistentEntityException("The adminCommunicateVendor with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            AdminCommunicateVendor adminCommunicateVendor;
            try {
                adminCommunicateVendor = em.getReference(AdminCommunicateVendor.class, id);
                adminCommunicateVendor.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The adminCommunicateVendor with id " + id + " no longer exists.", enfe);
            }
            Message messageId = adminCommunicateVendor.getMessageId();
            if (messageId != null) {
                messageId.getAdminCommunicateVendorList().remove(adminCommunicateVendor);
                messageId = em.merge(messageId);
            }
            Admin adminId = adminCommunicateVendor.getAdminId();
            if (adminId != null) {
                adminId.getAdminCommunicateVendorList().remove(adminCommunicateVendor);
                adminId = em.merge(adminId);
            }
            Vendor vendorId = adminCommunicateVendor.getVendorId();
            if (vendorId != null) {
                vendorId.getAdminCommunicateVendorList().remove(adminCommunicateVendor);
                vendorId = em.merge(vendorId);
            }
            em.remove(adminCommunicateVendor);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<AdminCommunicateVendor> findAdminCommunicateVendorEntities() {
        return findAdminCommunicateVendorEntities(true, -1, -1);
    }

    public List<AdminCommunicateVendor> findAdminCommunicateVendorEntities(int maxResults, int firstResult) {
        return findAdminCommunicateVendorEntities(false, maxResults, firstResult);
    }

    private List<AdminCommunicateVendor> findAdminCommunicateVendorEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(AdminCommunicateVendor.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public AdminCommunicateVendor findAdminCommunicateVendor(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(AdminCommunicateVendor.class, id);
        } finally {
            em.close();
        }
    }

    public int getAdminCommunicateVendorCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<AdminCommunicateVendor> rt = cq.from(AdminCommunicateVendor.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
