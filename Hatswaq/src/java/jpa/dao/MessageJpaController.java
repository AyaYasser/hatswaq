/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.dao;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.AdminCommunicateVendor;
import entity.Message;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.dao.exceptions.IllegalOrphanException;
import jpa.dao.exceptions.NonexistentEntityException;
import jpa.dao.exceptions.PreexistingEntityException;

/**
 *
 * @author Aya
 */
public class MessageJpaController implements Serializable {

    public MessageJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Message message) throws PreexistingEntityException, Exception {
        if (message.getAdminCommunicateVendorList() == null) {
            message.setAdminCommunicateVendorList(new ArrayList<AdminCommunicateVendor>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<AdminCommunicateVendor> attachedAdminCommunicateVendorList = new ArrayList<AdminCommunicateVendor>();
            for (AdminCommunicateVendor adminCommunicateVendorListAdminCommunicateVendorToAttach : message.getAdminCommunicateVendorList()) {
                adminCommunicateVendorListAdminCommunicateVendorToAttach = em.getReference(adminCommunicateVendorListAdminCommunicateVendorToAttach.getClass(), adminCommunicateVendorListAdminCommunicateVendorToAttach.getId());
                attachedAdminCommunicateVendorList.add(adminCommunicateVendorListAdminCommunicateVendorToAttach);
            }
            message.setAdminCommunicateVendorList(attachedAdminCommunicateVendorList);
            em.persist(message);
            for (AdminCommunicateVendor adminCommunicateVendorListAdminCommunicateVendor : message.getAdminCommunicateVendorList()) {
                Message oldMessageIdOfAdminCommunicateVendorListAdminCommunicateVendor = adminCommunicateVendorListAdminCommunicateVendor.getMessageId();
                adminCommunicateVendorListAdminCommunicateVendor.setMessageId(message);
                adminCommunicateVendorListAdminCommunicateVendor = em.merge(adminCommunicateVendorListAdminCommunicateVendor);
                if (oldMessageIdOfAdminCommunicateVendorListAdminCommunicateVendor != null) {
                    oldMessageIdOfAdminCommunicateVendorListAdminCommunicateVendor.getAdminCommunicateVendorList().remove(adminCommunicateVendorListAdminCommunicateVendor);
                    oldMessageIdOfAdminCommunicateVendorListAdminCommunicateVendor = em.merge(oldMessageIdOfAdminCommunicateVendorListAdminCommunicateVendor);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMessage(message.getId()) != null) {
                throw new PreexistingEntityException("Message " + message + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Message message) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Message persistentMessage = em.find(Message.class, message.getId());
            List<AdminCommunicateVendor> adminCommunicateVendorListOld = persistentMessage.getAdminCommunicateVendorList();
            List<AdminCommunicateVendor> adminCommunicateVendorListNew = message.getAdminCommunicateVendorList();
            List<String> illegalOrphanMessages = null;
            for (AdminCommunicateVendor adminCommunicateVendorListOldAdminCommunicateVendor : adminCommunicateVendorListOld) {
                if (!adminCommunicateVendorListNew.contains(adminCommunicateVendorListOldAdminCommunicateVendor)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain AdminCommunicateVendor " + adminCommunicateVendorListOldAdminCommunicateVendor + " since its messageId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<AdminCommunicateVendor> attachedAdminCommunicateVendorListNew = new ArrayList<AdminCommunicateVendor>();
            for (AdminCommunicateVendor adminCommunicateVendorListNewAdminCommunicateVendorToAttach : adminCommunicateVendorListNew) {
                adminCommunicateVendorListNewAdminCommunicateVendorToAttach = em.getReference(adminCommunicateVendorListNewAdminCommunicateVendorToAttach.getClass(), adminCommunicateVendorListNewAdminCommunicateVendorToAttach.getId());
                attachedAdminCommunicateVendorListNew.add(adminCommunicateVendorListNewAdminCommunicateVendorToAttach);
            }
            adminCommunicateVendorListNew = attachedAdminCommunicateVendorListNew;
            message.setAdminCommunicateVendorList(adminCommunicateVendorListNew);
            message = em.merge(message);
            for (AdminCommunicateVendor adminCommunicateVendorListNewAdminCommunicateVendor : adminCommunicateVendorListNew) {
                if (!adminCommunicateVendorListOld.contains(adminCommunicateVendorListNewAdminCommunicateVendor)) {
                    Message oldMessageIdOfAdminCommunicateVendorListNewAdminCommunicateVendor = adminCommunicateVendorListNewAdminCommunicateVendor.getMessageId();
                    adminCommunicateVendorListNewAdminCommunicateVendor.setMessageId(message);
                    adminCommunicateVendorListNewAdminCommunicateVendor = em.merge(adminCommunicateVendorListNewAdminCommunicateVendor);
                    if (oldMessageIdOfAdminCommunicateVendorListNewAdminCommunicateVendor != null && !oldMessageIdOfAdminCommunicateVendorListNewAdminCommunicateVendor.equals(message)) {
                        oldMessageIdOfAdminCommunicateVendorListNewAdminCommunicateVendor.getAdminCommunicateVendorList().remove(adminCommunicateVendorListNewAdminCommunicateVendor);
                        oldMessageIdOfAdminCommunicateVendorListNewAdminCommunicateVendor = em.merge(oldMessageIdOfAdminCommunicateVendorListNewAdminCommunicateVendor);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = message.getId();
                if (findMessage(id) == null) {
                    throw new NonexistentEntityException("The message with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Message message;
            try {
                message = em.getReference(Message.class, id);
                message.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The message with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<AdminCommunicateVendor> adminCommunicateVendorListOrphanCheck = message.getAdminCommunicateVendorList();
            for (AdminCommunicateVendor adminCommunicateVendorListOrphanCheckAdminCommunicateVendor : adminCommunicateVendorListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Message (" + message + ") cannot be destroyed since the AdminCommunicateVendor " + adminCommunicateVendorListOrphanCheckAdminCommunicateVendor + " in its adminCommunicateVendorList field has a non-nullable messageId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(message);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Message> findMessageEntities() {
        return findMessageEntities(true, -1, -1);
    }

    public List<Message> findMessageEntities(int maxResults, int firstResult) {
        return findMessageEntities(false, maxResults, firstResult);
    }

    private List<Message> findMessageEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Message.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Message findMessage(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Message.class, id);
        } finally {
            em.close();
        }
    }

    public int getMessageCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Message> rt = cq.from(Message.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
