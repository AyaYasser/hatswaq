/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.dao;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Branch;
import entity.BranchMobile;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import jpa.dao.exceptions.NonexistentEntityException;

/**
 *
 * @author Aya
 */
public class BranchMobileJpaController implements Serializable {

    public BranchMobileJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(BranchMobile branchMobile) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Branch brancheId = branchMobile.getBrancheId();
            if (brancheId != null) {
                brancheId = em.getReference(brancheId.getClass(), brancheId.getId());
                branchMobile.setBrancheId(brancheId);
            }
            em.persist(branchMobile);
            if (brancheId != null) {
                brancheId.getBranchMobileList().add(branchMobile);
                brancheId = em.merge(brancheId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(BranchMobile branchMobile) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            BranchMobile persistentBranchMobile = em.find(BranchMobile.class, branchMobile.getId());
            Branch brancheIdOld = persistentBranchMobile.getBrancheId();
            Branch brancheIdNew = branchMobile.getBrancheId();
            if (brancheIdNew != null) {
                brancheIdNew = em.getReference(brancheIdNew.getClass(), brancheIdNew.getId());
                branchMobile.setBrancheId(brancheIdNew);
            }
            branchMobile = em.merge(branchMobile);
            if (brancheIdOld != null && !brancheIdOld.equals(brancheIdNew)) {
                brancheIdOld.getBranchMobileList().remove(branchMobile);
                brancheIdOld = em.merge(brancheIdOld);
            }
            if (brancheIdNew != null && !brancheIdNew.equals(brancheIdOld)) {
                brancheIdNew.getBranchMobileList().add(branchMobile);
                brancheIdNew = em.merge(brancheIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = branchMobile.getId();
                if (findBranchMobile(id) == null) {
                    throw new NonexistentEntityException("The branchMobile with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            BranchMobile branchMobile;
            try {
                branchMobile = em.getReference(BranchMobile.class, id);
                branchMobile.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The branchMobile with id " + id + " no longer exists.", enfe);
            }
            Branch brancheId = branchMobile.getBrancheId();
            if (brancheId != null) {
                brancheId.getBranchMobileList().remove(branchMobile);
                brancheId = em.merge(brancheId);
            }
            em.remove(branchMobile);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<BranchMobile> findBranchMobileEntities() {
        return findBranchMobileEntities(true, -1, -1);
    }

    public List<BranchMobile> findBranchMobileEntities(int maxResults, int firstResult) {
        return findBranchMobileEntities(false, maxResults, firstResult);
    }

    private List<BranchMobile> findBranchMobileEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(BranchMobile.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public BranchMobile findBranchMobile(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(BranchMobile.class, id);
        } finally {
            em.close();
        }
    }

    public int getBranchMobileCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<BranchMobile> rt = cq.from(BranchMobile.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
